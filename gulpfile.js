var gulp = require('gulp');
var less = require('gulp-less');
var cleanCSS = require('gulp-clean-css');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();

var autoprefixerOptions = {
  browsers: ['last 2 versions']
};

gulp.task('browserSync', function() {
  browserSync.init({
    server: {
      baseDir: "./"
    },
    notify: false
  })
});

gulp.task('less', function() {
  return gulp.src('csstools.less')
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(autoprefixer(autoprefixerOptions))
    .pipe(cleanCSS())
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./css'))
    .pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('default', ['less', 'browserSync'], function() {
  gulp.watch('*.html').on('change', browserSync.reload);
  gulp.watch(['**/*.less'], ['less']);
});
